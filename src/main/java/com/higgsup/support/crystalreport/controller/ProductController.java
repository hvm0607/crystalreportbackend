package com.higgsup.support.crystalreport.controller;

import com.crystaldecisions.sdk.occa.report.lib.ReportSDKException;
import com.higgsup.support.crystalreport.dto.ReportParamsDTO;
import com.higgsup.support.crystalreport.service.IProductExportService;
import com.higgsup.support.crystalreport.service.IProductService;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping(value = "/api/products", method = RequestMethod.GET)
public class ProductController {

    private final IProductService productService;
    private final IProductExportService productExportService;

    public ProductController(IProductService productService, IProductExportService productExportService) {
        this.productService = productService;
        this.productExportService = productExportService;
    }


    @PostMapping("/report")
    public String generateReport(@RequestBody ReportParamsDTO reportParamssDTO) throws IOException, ReportSDKException {
        return productExportService.exportProducts(reportParamssDTO);
    }
}
