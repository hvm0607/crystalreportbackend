package com.higgsup.support.crystalreport.repo;

import com.higgsup.support.crystalreport.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product,String> {
    Page<Product> findAll(Pageable pageable);
}
