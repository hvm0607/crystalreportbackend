package com.higgsup.support.crystalreport.service;

import com.higgsup.support.crystalreport.dto.ProductDTO;

import java.util.List;

public interface IProductService {
    List<ProductDTO> getAllProducts();
}
