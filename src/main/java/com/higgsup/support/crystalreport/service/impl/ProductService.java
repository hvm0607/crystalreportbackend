package com.higgsup.support.crystalreport.service.impl;

import com.higgsup.support.crystalreport.dto.ProductDTO;
import com.higgsup.support.crystalreport.entity.Product;
import com.higgsup.support.crystalreport.repo.ProductRepository;
import com.higgsup.support.crystalreport.service.IProductService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional(rollbackOn = Exception.class)
public class ProductService implements IProductService {

    private final ProductRepository productepository;
    private final MapperFacade objMapper;

    public ProductService(ProductRepository productepository, MapperFacade objMapper) {
        this.productepository = productepository;
        this.objMapper = objMapper;
    }


    @Override
    public List<ProductDTO> getAllProducts() {
        Iterable<Product> products = productepository.findAll();
        return objMapper.mapAsList(products,ProductDTO.class);
    }
}
