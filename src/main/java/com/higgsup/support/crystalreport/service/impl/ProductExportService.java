package com.higgsup.support.crystalreport.service.impl;

import com.crystaldecisions.reports.sdk.ParameterFieldController;
import com.crystaldecisions.reports.sdk.ReportClientDocument;
import com.crystaldecisions.sdk.occa.report.application.OpenReportOptions;
import com.crystaldecisions.sdk.occa.report.exportoptions.ReportExportFormat;
import com.higgsup.support.crystalreport.dto.ProductDTO;
import com.higgsup.support.crystalreport.dto.ReportParamsDTO;
import com.higgsup.support.crystalreport.exception.ServiceException;
import com.higgsup.support.crystalreport.service.IProductExportService;
import com.higgsup.support.crystalreport.service.IProductService;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.InputStream;
import java.util.List;

@Service
public class ProductExportService implements IProductExportService{
    private final IProductService productService;

    public ProductExportService(IProductService productService) {
        this.productService = productService;
    }

    @Override
    public String exportProducts(ReportParamsDTO reportParamsDTO) {
        try {
            // Open Report
            ReportClientDocument reportClientDocument = new ReportClientDocument();
            reportClientDocument.open("Report/crystalReport/template/products.rpt", OpenReportOptions._openAsReadOnly);

            // blind data
            final List<ProductDTO> listProduct = productService.getAllProducts();
            reportClientDocument.getDatabaseController().setDataSource(listProduct,ProductDTO.class,"products","products");

            ParameterFieldController parameterFieldController = reportClientDocument.getDataDefController().getParameterFieldController();
            parameterFieldController.setCurrentValue("","firstName",reportParamsDTO.getFirstName());
            parameterFieldController.setCurrentValue("","lastName",reportParamsDTO.getLastName());
            parameterFieldController.setCurrentValue("","title",reportParamsDTO.getTitle());

            //push to PDF
            final InputStream reportInputStream = reportClientDocument.getPrintOutputController().export(ReportExportFormat.PDF);
            String fileName = "products.pdf";
            FileUtils.copyInputStreamToFile(reportInputStream,new File("report/pdf/" + fileName));

            return fileName;

        } catch ( Exception ex) {
            throw  new ServiceException(" Generate Reprot faile ", ex );
        }
    }
}
