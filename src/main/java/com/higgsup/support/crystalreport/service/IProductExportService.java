package com.higgsup.support.crystalreport.service;

import com.crystaldecisions.sdk.occa.report.lib.ReportSDKException;
import com.higgsup.support.crystalreport.dto.ReportParamsDTO;

import java.io.IOException;

public interface IProductExportService {
    String exportProducts (ReportParamsDTO reportParamsDTO) throws ReportSDKException, IOException;
}
