package com.higgsup.support.crystalreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrystalreportApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrystalreportApplication.class, args);
	}
}
